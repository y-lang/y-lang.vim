" Vim Syntax File
" Language:     Y
" Creator:      Gregoire Lejeune

if version < 600
    syntax clear
elseif exists("b:current_syntax")
    finish
endif

syntax case match

syntax keyword yTodo TODO XXX FIXME NOTE

syntax keyword yType true false null if else return import

syntax keyword yKeyword fn if else return

syntax match yOperator "\v\=\="
syntax match yOperator "\v<\="
syntax match yOperator "\v>\="
syntax match yOperator "\v!\="
syntax match yOperator "\v<"
syntax match yOperator "\v>"
syntax match yOperator "\v!"
syntax match yOperator "\v\+"
syntax match yOperator "\v-"
syntax match yOperator "\v\*"
syntax match yOperator "\v/"
syntax match yOperator "\v:\="
syntax match yOperator "\v\="
syntax match yOperator "\v&"
syntax match yOperator "\v\|"
syntax match yOperator "\v^"
syntax match yOperator "\v\~"
syntax match yOperator "\v&&"
syntax match yOperator "\v\|\|"

syntax region yComment start='#' end='$' keepend
syntax region yComment start='//' end='$' keepend

syntax match yNumber "\v<\d+>"
syntax match yNumber "\v<\d+\.\d+>"

syntax match yBoolean "\vtrue"
syntax match yBoolean "\vfalse"

syntax match ySpecial "\v\\." contained
syntax region yString start=/"/ skip=/\\./ end=/"/ oneline contains=yInterpolatedWrapper,ySpecial
syntax region yInterpolatedWrapper start="\v\{\s*" end="\v\s*\}" contained containedin=yString contains=yInterpolatedString
syntax match yInterpolatedString "\v\w+(\{\})?" contained containedin=yInterpolatedWrapper

highlight link yTodo Todo
highlight link yType Type
highlight link yKeyword Keyword
highlight link yString String
highlight link yComment Comment
highlight link yOperator Operator
highlight link yNumber Number
highlight link yBoolean Boolean
highlight link yInterpolatedWrapper Delimiter
highlight link ySpecial Special

let b:current_syntax = "y"
